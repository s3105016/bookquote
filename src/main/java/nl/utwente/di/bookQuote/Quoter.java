package nl.utwente.di.bookQuote;

import java.util.HashMap;

public class Quoter {

    HashMap<String, Double> bookQuote = new HashMap<>();
    {
        bookQuote.put("1", 10.0);
        bookQuote.put("2", 45.0);
        bookQuote.put("3", 20.0);
        bookQuote.put("4", 35.0);
        bookQuote.put("5", 50.0);
    }

    double getBookPrice(String isbn) {
        return bookQuote.getOrDefault(isbn, 0.0);
    }
}
